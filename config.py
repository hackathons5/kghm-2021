from pathlib import Path

ROOT_DIR = Path(".")

DATA_DIR = ROOT_DIR / "data"
DATA_RAW_DIR = DATA_DIR / "raw"
DATA_PROCESSED_DIR = DATA_DIR / "processed"
DATA_ENGINEERED_DIR = DATA_DIR / "engineered"
