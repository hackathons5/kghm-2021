import numpy as np

import pandas as pd
from tqdm import tqdm
from sklearn.model_selection import train_test_split

from src.models import LGBM
import src.utils as utils

lk_variables = [
    "BREAKP",
    "ENGCOOLT",
    "ENGOILP",
    "ENGOILT",
    "ENGRPM",
    "ERROR",
    "FUELUS",
    "GROILP",
    "GROILT",
    "HYDOILP",
    "HYDOILT",
    "INTAKEP",
    "INTAKET",
    "SELGEAR",
    "SPEED",
]

wos_variables = [
    "BREAKP",
    "ENGCOOLT",
    "ENGOILP",
    "ENGRPM",
    "ENGTPS",
    "ERROR",
    "FUELUS",
    "GROILP",
    "GROILT",
    "HYDOILP",
    "HYDOILT",
    "INTAKEP",
    "INTAKET",
    "SELGEAR",
    "SPEED",
    "TEMPIN",
    "TRNAUT",
    "TRNBPS",
    "TRNLUP",
]


def train_model(fm, grouping_freq="30s", window_size=3):

    selected_lk_variables = [
        *lk_variables,
        *[
            ent
            for ent in sorted(
                fm.loader.engineered_data["middle"].columns
                if fm.loader.engineered_data is not None
                else []
            )
        ],
    ]

    selected_wos_variables = [
        *wos_variables,
        *[
            ent
            for ent in sorted(
                fm.loader.engineered_data["middle"].columns
                if fm.loader.engineered_data is not None
                else []
            )
        ],
    ]

    machine_data = fm.loader.get_all_data()

    if fm.machine_id[:2] == "LK":
        selected_columns = selected_lk_variables
    else:
        selected_columns = selected_wos_variables

    selected_columns = list(set(selected_columns))

    def prepare_dataset(data, window_size, grouping_freq, selected_columns):
        data = data[sorted(list(set(["czas", *selected_columns])))]
        data = utils.get_time_grouped_data(data, freq=grouping_freq)
        data = data.reset_index()
        data["calculate"] = [(x % window_size == 0) for x in range(0, len(data))]
        dataset = []
        tmp_var = list(range(-window_size + 1, 1))
        for idx in tqdm(data.index):
            if (
                idx > window_size
                and data.loc[idx, "calculate"]
                and (
                    (data.loc[idx, "czas"] - data.loc[idx - window_size, "czas"])
                    == pd.Timedelta(grouping_freq) * window_size
                )
            ):
                tmp = (
                    data.loc[(idx - window_size + 1) : idx, :]
                    .drop(columns=["czas", "calculate"])
                    .fillna(method="bfill")
                    .fillna(0)
                )
                tmp["idx"] = 1
                tmp["index"] = tmp_var
                tmp = tmp.pivot(index="idx", columns="index")
                tmp.columns = [
                    "_".join([str(col_lv) for col_lv in col])
                    for col in tmp.columns.values
                ]
                dataset.append(tmp)
        return pd.concat(dataset)

    fault_data = machine_data["last"]
    fault_data = fault_data[
        fault_data["czas"] > (fault_data.tail(1)["czas"] - pd.Timedelta("5D")).values[0]
    ]

    true_data = prepare_dataset(
        fault_data, window_size, grouping_freq, selected_columns
    )
    true_labels = np.ones(len(true_data))

    good_data = machine_data["middle"]
    good_data = good_data[
        good_data["czas"] > (good_data.tail(1)["czas"] - pd.Timedelta("4W")).values[0]
    ]

    false_data = prepare_dataset(
        good_data, window_size, grouping_freq, selected_columns
    ).sample(len(true_data))

    false_labels = np.zeros(len(false_data))

    all_data = pd.concat([true_data, false_data])
    all_labels = np.concatenate((true_labels, false_labels))

    print(all_labels.shape)
    print(all_data.shape)

    X_train, X_val, y_train, y_val = train_test_split(
        all_data, all_labels, test_size=0.2
    )

    model = LGBM()

    best_params = model.find_best_params(X_train, y_train, X_val, y_val)

    model.train(X_train, y_train, best_params)

    return model, X_val, y_val
