import os

from glob import glob

import pandas as pd

import config as cfg
import src.utils as utils


class DataManager:
    def __init__(self, raw_path=None, processed_path=None) -> None:
        if raw_path is None:
            raw_path = cfg.DATA_RAW_DIR
        if processed_path is None:
            processed_path = cfg.DATA_PROCESSED_DIR
        self.raw_path = raw_path
        self.processed_path = processed_path

    def find_available_data(self):
        return pd.DataFrame(
            [
                os.path.split(name)[-1][:-4].split(sep="_")[-3:]
                for name in self.processed_path.glob("final_machine*.csv")
            ],
            columns=["maszyna", "rok", "miesiac"],
        )

    def find_unprepared_data(self):
        return pd.DataFrame(
            [
                os.path.split(name)[-1][:-4].split(sep="_")[-3:]
                for name in self.processed_path.glob("pivot_machine*.csv")
            ],
            columns=["maszyna", "rok", "miesiac"],
        )

    def recalculate_final(self, machine_id, year, month):
        data = self.loader.get_pivot_data(machine_id, year, month)
        final_data = utils.prepare_full_range(data)
        final_data.to_csv(
            self.processed_path / f"final_machine_{machine_id}_{year}_{month}.csv",
            index=False,
        )

    def recalculate_all_unprepared(self):
        for combination in self.find_unprepared_data().itertuples():
            self.recalculate_final(combination[1], combination[2], combination[3])

    def list_available_machines(self):
        return {
            "processed": [
                os.path.split(machine)[-1][8:-4]
                for machine in glob(str(self.processed_path / "machine_*.csv"))
            ],
            "raw": [
                ent
                for ent in os.listdir(self.raw_path)
                if os.path.isdir(self.raw_path / ent)
            ],
            "pivot": [
                os.path.split(machine)[-1][14:-4]
                for machine in glob(str(self.processed_path / "pivot_machine_*.csv"))
            ],
            "final": [
                os.path.split(machine)[-1][14:-4]
                for machine in glob(str(self.processed_path / "final_machine_*.csv"))
            ],
        }
