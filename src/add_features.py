import pickle
import pandas as pd
import datetime
import numpy as np

from abc import ABC, abstractmethod
from pyod.models.cblof import CBLOF

from src.loaders import CSVLoader
import config as cfg
import src.utils as utils


class FeatureManager:
    def __init__(self, machine_id=None):
        self.loader = CSVLoader()
        if machine_id is not None:
            self.machine_id = machine_id
            self.loader.load_all_data(machine_id)
            self.loader.load_engineered_data(machine_id)
        self.paired_features = {
            "korelacja": CorrelationFinder
        }

    def load_machine_data(self, machine_id):
        self.loader.load_all_data(machine_id)
        self.loader.load_engineered_data(machine_id)
        self.machine_id = machine_id

    def test_feature(self, feature):

        self.current_feature = feature

        return {
            stage: feature.calculate_feature(self.loader.get_all_data()[stage])
            for stage in ["middle", "last"]
        }

    def save_current_feature(self):
        for machine_id in self.loader.data_manager.list_available_machines()["raw"]:
            try:
                self.loader.load_all_data(machine_id)
                engineered_data = self.loader.load_engineered_data(machine_id)
                print(machine_id)
                new_features = self.test_feature(self.current_feature)

                if engineered_data is None:
                    pickle.dump(
                        new_features,
                        open(cfg.DATA_ENGINEERED_DIR / f"{machine_id}.pkl", "wb"),
                    )
                else:
                    print(f"Adding new data to {machine_id}")
                    pickle.dump(
                        {
                            stage: new_features[stage].merge(
                                engineered_data[stage], on="czas", how="outer"
                            )
                            for stage in ["middle", "last"]
                        },
                        open(cfg.DATA_ENGINEERED_DIR / f"{machine_id}.pkl", "wb"),
                    )
            except Exception:
                print(f"Something went wrong for {machine_id}")
                pass

        self.loader.load_all_data(self.machine_id)
        self.loader.load_engineered_data(self.machine_id)


class Feature(ABC):
    @abstractmethod
    def calculate_feature(pivoted_data):
        pass


class CorrelationFinder(Feature):
    def __init__(self, feature_pairs, freq):
        self.freq = freq
        self.feature_pairs = feature_pairs

    @staticmethod
    def find_correlation(dataframe, feature_pairs):
        try:
            return pd.DataFrame(
                dict(
                    czas=[dataframe["czas"].iloc[-1]],
                    **{
                        f"corr_{feature1}_{feature2}": [
                            dataframe[feature1].corr(dataframe[feature2])
                        ]
                        for feature1, feature2 in feature_pairs
                    },
                )
            )
        except Exception:
            return None

    def calculate_feature(self, pivoted_data):
        features = pd.concat(
            [
                self.find_correlation(group, self.feature_pairs)
                for _, group in pivoted_data.groupby(
                    pd.Grouper(key="czas", freq=self.freq)
                )
            ]
        )
        return features


class VarianceCalculator(Feature):
    def __init__(self, feature_name, freq):
        self.freq = freq
        self.feature_name = feature_name

    @staticmethod
    def find_variance(dataframe, feature_name):
        try:
            return pd.DataFrame(
                {
                    "czas": [dataframe["czas"].iloc[-1]],
                    f"var_{feature_name}": [dataframe[feature_name].var()],
                },
            )
        except Exception:
            return None

    def calculate_feature(self, pivoted_data):
        features = pd.concat(
            [
                self.find_variance(group, self.feature_name)
                for _, group in pivoted_data.groupby(
                    pd.Grouper(key="czas", freq=self.freq)
                )
            ]
        )
        return features


class OutlierFinder(Feature):
    def __init__(self, feature_pairs, freq):
        self.freq = freq
        self.feature_pair = feature_pairs[0]
        self.clf = CBLOF(contamination=0.01, check_estimator=False, random_state=0)

    def calculate_feature(self, pivoted_data):
        feature1, feature2 = self.feature_pair

        data_to_detect = (
            utils.get_time_grouped_data(pivoted_data, freq=self.freq)
            .reset_index()[["czas", feature1, feature2]]
            .dropna()
            .copy()
        )

        self.clf.fit(data_to_detect.drop(columns="czas").values)

        y_pred = self.clf.predict(data_to_detect.drop(columns="czas").values)
        data_to_detect[f"outlier_{feature1}_{feature2}"] = y_pred

        return data_to_detect[["czas", f"outlier_{feature1}_{feature2}"]]


class BreakTimer(Feature):
    @staticmethod
    def calculate_feature(pivoted_data):

        timer_data = pivoted_data[["czas"]].copy()

        timer_data["bigger_stop"] = np.nan
        timer_data.loc[
            (timer_data["czas"].diff() > datetime.timedelta(days=1)).dropna(),
            "bigger_stop",
        ] = timer_data.loc[
            (timer_data["czas"].diff() > datetime.timedelta(days=1)).dropna(), "czas"
        ]
        timer_data["bigger_stop"] = (
            timer_data["bigger_stop"]
            .fillna(method="ffill")
            .fillna(timer_data["czas"].loc[0])
        )

        timer_data["czas_od_postoju"] = (
            timer_data["czas"] - timer_data["bigger_stop"]
        ).dt.days * (60 * 60 * 24) + (
            timer_data["czas"] - timer_data["bigger_stop"]
        ).dt.seconds

        return timer_data[["czas", "czas_od_postoju"]]
