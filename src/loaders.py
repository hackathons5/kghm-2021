import itertools
import json
import os
import pickle

from abc import ABC
from glob import glob
from multiprocessing import Pool

# from zipfile import ZipFile
import numpy as np
import pandas as pd

import config as cfg

import src.utils as utils

from src.data_manager import DataManager


class Loader(ABC):
    def get_machine_data(self, machine_id):
        pass

    def prepare_machine_data(self, machine_id):
        pass


class CSVLoader(Loader):
    def __init__(self, raw_path=None, processed_path=None):
        self.engineered_data = None
        self.error_codes = {}
        self.error_decodes = {}
        self.current_code = 1
        self.data_manager = DataManager(
            raw_path=raw_path, processed_path=processed_path
        )

        # odpal funkcję, która dodaje self.all_months - coś z czego korzysta metoda
        # self.load_all_data(machine_id)
        self.create_data_with_all_the_months()

    def create_data_with_all_the_months(self):
        """Wygeneruj datafrejm z listą wszystkich miesięcy, które mamy załadowane dla
        każdej z maszyn wraz z opisem czy jest to środek okresu eksploatacji, czy koniec
        """
        last_months = {}
        months = []
        for machine_id in self.data_manager.list_available_machines()["raw"]:
            for machine_year in [
                full_path.split("/")[-1]
                for full_path in glob(f"data/raw/{machine_id}/*")
            ]:

                months.append(
                    {
                        "machine_id": machine_id,
                        "year": machine_year,
                        "month": [
                            full_path.split("/")[-1]
                            for full_path in glob(
                                f"data/raw/{machine_id}/{machine_year}/*"
                            )
                        ],
                    }
                )

        months = (
            pd.DataFrame(months)
            .explode("month")
            .sort_values(by=["machine_id", "year", "month"])
            .reset_index(drop=True)
        )

        middle_months = (
            months.groupby("machine_id").head(6).groupby("machine_id").tail(3).copy()
        )
        middle_months["type"] = "middle"

        last_months = months.groupby("machine_id").tail(3).copy()
        last_months["type"] = "last"

        all_months = (
            pd.concat([last_months, middle_months])
            .sort_values(by=["machine_id", "year", "month"])
            .reset_index(drop=True)
        )
        self.all_months = all_months

    def load_all_data(self, machine_id):
        all_data = {
            group: pd.concat(
                [
                    self.get_pivot_data(
                        machine_id, row[2], row[3], data=None, recalculate=False
                    )
                    for row in machine_data.itertuples()
                ]
            ).reset_index(drop=True)
            for group, machine_data in self.all_months[
                self.all_months["machine_id"] == machine_id
            ].groupby("type")
        }

        self.all_data = all_data

        return all_data

    def load_engineered_data(self, machine_id):
        try:
            engineered_data = pickle.load(
                open(cfg.DATA_ENGINEERED_DIR / f"{machine_id}.pkl", "rb")
            )

            self.engineered_data = engineered_data
            return engineered_data
        except Exception:
            self.engineered_data = None
            return None

    def get_all_data(self):
        if self.engineered_data is not None:
            return {
                stage: self.all_data[stage].merge(
                    self.engineered_data[stage], on="czas", how="outer"
                )
                for stage in ["middle", "last"]
            }
        else:
            return self.all_data

    def encode_error(self, code, number):
        if (code, number) not in self.error_codes:
            encoding = (str(self.current_code) + "0000")[:4]
            self.error_codes[(code, number)] = encoding
            self.error_decodes[encoding] = (code, number)
            self.current_code += 1
        return self.error_codes[(code, number)]

    def decode_error(self, encodings):
        decodings = [
            self.error_decodes[encoding]
            for encoding in [
                encodings[i : i + 4] for i in range(0, len(str(round(encodings))), 4)
            ]
        ]
        return decodings

    def create_all_encodings(self, data):
        combinations = data[data["typ_danych"] == "E"][
            ["zmienna", "wartosc"]
        ].drop_duplicates()

        for combination in combinations.itertuples():
            self.encode_error(combination[1], combination[2])

    def translate_codes(self, data):
        data2 = data[data["typ_danych"] == "E"]
        data = data[data["typ_danych"] != "E"]
        data2.loc[:, "wartosc"] = [
            self.error_codes[combination]
            for combination in zip(data2["zmienna"].values, data2["wartosc"].values)
        ]

        def agg_code(vals):
            return float("".join([str(val) for val in vals]))

        data2 = data2.groupby(by="czas")["wartosc"].apply(agg_code).reset_index()
        data2["zmienna"] = "ERROR"
        data2["typ_danych"] = "E"

        return pd.concat([data, data2])

    def count_errors(self, data):
        data2 = data[data["typ_danych"] == "E"]
        data = data[data["typ_danych"] != "E"]

        data2 = data2.groupby(by="czas")["wartosc"].count().reset_index()

        # data2.loc[:, "wartosc"] = data2["wartosc"].astype(float)
        data2["zmienna"] = "ERROR"
        data2["typ_danych"] = "E"

        return pd.concat([data, data2])

    def get_machine_data(self, machine_id, year, month, recalculate=False):
        if (
            f"{machine_id}_{year}_{month}"
            not in self.data_manager.list_available_machines()["processed"]
            or recalculate
        ):
            try:
                data, _, _, _ = self.prepare_machine_data(machine_id, year, month)
            except Exception as e:
                print(e)
                print("No preprocessed or raw data related to the machine")
                return None
        else:
            data = pd.read_csv(
                cfg.DATA_PROCESSED_DIR / f"machine_{machine_id}_{year}_{month}.csv",
                index_col="idx",
            )
        data["czas"] = pd.to_datetime(data["czas"])
        return data

    def get_pivot_data(self, machine_id, year, month, data=None, recalculate=False):
        if (
            f"{machine_id}_{year}_{month}"
            not in self.data_manager.list_available_machines()["pivot"]
            or recalculate
        ):
            if data is None:
                print(f"Loaded data for {machine_id}_{year}_{month} is empty, skipping")
                return None
            # data = data.fillna(-1000.0)
            data = (
                data.pivot_table(
                    index="czas",
                    columns="zmienna",
                    values="wartosc",
                    aggfunc=np.nanmean,
                )
                .sort_index()
                .reset_index()
            )
            data.to_csv(
                cfg.DATA_PROCESSED_DIR
                / f"pivot_machine_{machine_id}_{year}_{month}.csv",
                index_label="idx",
            )
        else:
            data = pd.read_csv(
                cfg.DATA_PROCESSED_DIR
                / f"pivot_machine_{machine_id}_{year}_{month}.csv",
                index_col="idx",
            )
        data["czas"] = pd.to_datetime(data["czas"])
        return data

    def get_final_data(
        self, machine_id, year, month, use_fill=False, recalculate=False
    ):
        if (
            f"{machine_id}_{year}_{month}"
            not in self.data_manager.list_available_machines()["final"]
            or recalculate
        ):
            data = self.recalculate_final(
                self, machine_id, year, month, use_fill=use_fill
            )
        else:
            data = pd.read_csv(
                self.data_manager.processed_path
                / f"final_machine_{machine_id}_{year}_{month}.csv",
            )
            data["czas"] = pd.to_datetime(data["czas"])
        return data

    def prepare_machine_data(self, machine_id, year, month):

        load_func = load_archives_4
        if machine_id == "LK3 045L":
            load_func = load_archives_5

        year_val = year if year != "all" else "*"
        month_val = month if month != "all" else "*"
        paths = glob(
            str(cfg.DATA_RAW_DIR / f"{machine_id}/{year_val}/{month_val}/*.zip")
        )

        metadata = {
            "typ_maszyny": os.path.split(paths[0])[-1][7:10],
            "numer_maszyny": os.path.split(paths[0])[-1][13:16],
            "oddzial": os.path.split(paths[0])[-1][16:17],
        }

        with Pool(8) as pool:
            results = list(pool.imap(load_func, paths))

        results = pd.concat(list(itertools.chain(*results))).reset_index(drop=True)
        # results.columns = [
        #     "czas",
        #     "nazwa",
        #     "typ_operacji",
        #     "typ_wartosci",
        #     "wartosc",
        #     "kod_jakosci",
        # ]

        # parse values from the main code column
        results["oddzial"] = results["nazwa"].str[1:2].str.strip(to_strip="_")
        results["typ_danych"] = results["nazwa"].str[2:3].str.strip(to_strip="_")
        results["typ_maszyny"] = results["nazwa"].str[7:12].str.strip(to_strip="_")
        results["numer_maszyny"] = results["nazwa"].str[12:16].str.strip(to_strip="_")
        results["oddzial_2"] = results["nazwa"].str[16:17].str.strip(to_strip="_")
        results["zmienna"] = results["nazwa"].str[17:28].str.strip(to_strip="_")
        results["typ_zmiennej"] = results["nazwa"].str[28:29].str.strip(to_strip="_")
        results["jednostka"] = results["nazwa"].str[29:32].str.strip(to_strip="_")

        results.drop(columns=["nazwa"], inplace=True)

        wrong_results = results[results["oddzial"] != results["oddzial_2"]]
        wrong_results = pd.concat(
            [
                wrong_results,
                *[
                    results[results[col_name] != value]
                    for col_name, value in metadata.items()
                ],
            ]
        )
        additional_cols = ["typ_wartosci", "typ_operacji"]
        additional_results = results[additional_cols]

        results = results.iloc[results.index.difference(wrong_results.index)].drop(
            columns=[
                "oddzial_2",
                "typ_zmiennej",
                "jednostka",
                "kod_jakosci",
                *[key for key in metadata.keys()],
                *additional_cols,
            ]
        )

        # self.create_all_encodings(results)
        # results = self.translate_codes(results)

        results = self.count_errors(results)

        results.loc[:, "wartosc"] = pd.to_numeric(results["wartosc"], errors='coerce')

        results.to_csv(
            cfg.DATA_PROCESSED_DIR / f"machine_{machine_id}_{year}_{month}.csv",
            index_label="idx",
        )
        wrong_results.to_csv(
            cfg.DATA_PROCESSED_DIR / f"errors_machine_{machine_id}_{year}_{month}.csv",
            index=False,
        )
        additional_results.to_csv(
            cfg.DATA_PROCESSED_DIR
            / f"supplement_machine_{machine_id}_{year}_{month}.csv",
            index_label="idx",
        )
        with open(
            cfg.DATA_PROCESSED_DIR
            / f"metadata_machine_{machine_id}_{year}_{month}.json",
            "w",
        ) as fhandle:
            json.dump(metadata, fhandle)

        return results, additional_results, wrong_results, metadata

    def recalculate_final(self, machine_id, year, month, use_fill=False):
        data = self.get_pivot_data(machine_id, year, month)
        final_data = utils.prepare_full_range(data, use_fill=use_fill)
        final_data.to_csv(
            self.data_manager.processed_path
            / f"final_machine_{machine_id}_{year}_{month}.csv",
            index=False,
        )
        return final_data

    def recalculate_all_unprepared(self):
        for combination in self.data_manager.find_unprepared_data().itertuples():
            self.recalculate_final(combination[1], combination[2], combination[3])


load_archives_4 = utils.load_archives(skip_rows=4)
load_archives_5 = utils.load_archives(skip_rows=6)


def load_awarie():
    # ładownie ręcznie przepisanego pliku awarie.csv
    awarie = pd.read_csv(cfg.DATA_PROCESSED_DIR / "awarie.csv")

    # ustal typ zmiennych
    awarie["zgłoszenie_awarii"] = pd.to_datetime(awarie["zgłoszenie_awarii"])
    awarie["praca_od"] = pd.to_datetime(awarie["praca_od"])
    awarie["praca_do"] = pd.to_datetime(awarie["praca_do"])

    return awarie
