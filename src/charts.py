# import plotly.express as px
import plotly.graph_objects as go


def plot_compare_ts(data, columns):

    fig = go.Figure([go.Scatter(x=data["czas"], y=data[columns[0]], name=columns[0])])
    for col in columns[1:]:
        fig.add_trace(go.Scatter(x=data["czas"], y=data[col], name=col))
    fig.update_xaxes(rangeslider_visible=True)
    fig.update_layout(
        title=f"Porównanie {columns}",
    )
    #  fig.update_xaxes(
    #     dtick="M1",
    #     tickformat="%b\n%Y",
    #     ticklabelmode="period")
    return fig
