import inspect

from os.path import dirname

import sys

import pandas as pd
import streamlit as st

# from src.data_manager import DataManager
st.set_page_config(layout="wide")
this_dir = inspect.getfile(inspect.currentframe())
sys.path.append(dirname(dirname(this_dir)))

import src.charts as charts

# from src.loaders import CSVLoader
from src.add_features import FeatureManager

col1, col2, col3 = st.beta_columns([1, 4, 2])


@st.cache(allow_output_mutation=True)
def get_feature_manager():
    return FeatureManager()


feature_manager = get_feature_manager()


@st.cache
def load_available_data():
    return feature_manager.loader.data_manager.find_unprepared_data()


available_data = load_available_data()


@st.cache
def load_selected_data(machine_id, year, month):
    return feature_manager.loader.get_pivot_data(
        machine_id=machine_id, year=year, month=month
    )


@st.cache
def load_full_data(machine_id):
    feature_manager.load_machine_data(machine_id)
    return feature_manager.loader.get_all_data()


with col1:
    data_view = st.radio("Wybierz typ przegladania okresów", ["miesiecznie", "okresy"])

    if data_view == "miesiecznie":
        machine_id = st.selectbox(
            "Wybierz maszynę", sorted(pd.unique(available_data["maszyna"]).tolist())
        )
        selected_machine = available_data[available_data["maszyna"] == machine_id]

        year = st.selectbox(
            "Wybierz rok",
            sorted(pd.unique(selected_machine["rok"]).tolist()),
        )

        selected_year = selected_machine[selected_machine["rok"] == year]

        month = st.selectbox(
            "Wybierz miesiac",
            sorted(selected_year["miesiac"].to_list()),
        )

        selected_data = {"miesiac": load_selected_data(machine_id, year, month)}
        cols = selected_data["miesiac"].columns

    elif data_view == "okresy":

        machine_id = st.selectbox(
            "Wybierz maszynę",
            sorted(pd.unique(feature_manager.loader.all_months["machine_id"]).tolist()),
        )

        full_data = load_full_data(machine_id)

        # period = st.selectbox("Wybierz okres", ["middle", "last"])

        selected_data = full_data
        cols = selected_data["middle"].columns

    param1 = st.selectbox(
        "Wybierz pierwszy parametr",
        sorted(cols),
    )

    param2 = st.selectbox(
        "Wybierz drugi parametr",
        sorted(cols),
    )

    params = list(set([param1, param2]))

    freq = st.selectbox(
        "Wybierz częstotliwość grupowania",
        ["D", "H", "15min", "5min", "1min", "15S", "S"],
    )

    func = st.selectbox(
        "Wybierz funkcję grupującą",
        ["średnia", "max"],
    )

    pair_feat = st.selectbox(
        "Wybierz transformację",
        ["-", *feature_manager.paired_features.keys()],
    )

    if pair_feat != "-" and len(params) > 1:
        selected_data = feature_manager.test_feature(
            feature_manager.paired_features[pair_feat]([(param1, param2)], freq=freq)
        )
        params = [list(selected_data.values())[0].columns[1]]


#     with st.beta_expander("Relevant experience"):
#         for emp_id, emp_slug in zip(emp_ids, emp_slugs):
#             st.write(emp_slug)
#             for cat in categories2:
#                 content = past_projects_parent_CE_presenter.get(emp_id, cat)
#                 content = content.groupby(by=["epe_pro_id"])[["epe_ce_code"]].agg(
#                     lambda x: ", ".join(x)
#                 )
#                 content.reset_index(inplace=True)
#                 if len(content) > 0:
#                     st.write(
#                         cat,
#                         ", Gained in projects: ",
#                         [
#                             f"{row['epe_pro_id']} - "
#                             f"{str(projects[projects['pro_id'] == row['epe_pro_id']]['pro_short_title'].values[0])}"
#                             f": {row['epe_ce_code']}"
#                             for idx, row in content.iterrows()
#                         ],
#                     )

# weights = [self_assigned, self_assigned_group, historical, historical_group]

with col2:
    try:
        for name, data in selected_data.items():
            st.write(name)
            if func == "średnia":
                grouped_data = (
                    data[["czas", *params]]
                    .groupby(pd.Grouper(key="czas", freq=freq))
                    .mean()
                    .reset_index()
                )
            elif func == "max":
                grouped_data = (
                    data[["czas", *params]]
                    .groupby(pd.Grouper(key="czas", freq=freq))
                    .max()
                    .reset_index()
                )
            fig = charts.plot_compare_ts(data=grouped_data, columns=params)
            st.plotly_chart(
                fig,
                use_container_width=True,
            )

    except Exception as e:
        st.write(e)

with col3:
    try:
        st.write("Placeholder")
        # st.dataframe(data=grouped_data)
    except Exception as e:
        st.write(e)
