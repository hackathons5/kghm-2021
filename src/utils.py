from toolz.functoolz import curry
from zipfile import ZipFile

import pandas as pd


@curry
def load_archives(path, skip_rows):
    with ZipFile(path) as zip_obj:
        archives = []
        for data in zip_obj.infolist():
            try:
                new_archive = pd.read_csv(
                    zip_obj.open(data),
                    sep="|",
                    skiprows=skip_rows,
                    header=None,
                    parse_dates=[[2, 3]],
                )
                archives.append(new_archive)
            except Exception as e:
                print(e)
                print(f"Error with file {data.filename} Skipping")

        archives = [change_file(data) for data in archives]
    return archives


def change_file(data):
    data.columns = [
        "czas",
        "nazwa",
        "typ_operacji",
        "typ_wartosci",
        "wartosc",
        "kod_jakosci",
    ]
    data = data[data["kod_jakosci"] == 192]
    return data


def prepare_full_range(data, use_fill=False):
    whole_range = pd.DataFrame(
        pd.date_range(min(data["czas"]), max(data["czas"]), freq="S"), columns=["czas"]
    )
    full_ts = pd.merge(whole_range, data, how="left", on="czas")
    errors = full_ts["ERROR"].to_list()
    if use_fill:
        full_ts = full_ts.fillna(method="backfill")
    full_ts.loc[:, "ERROR"] = errors
    return full_ts


def get_time_grouped_data(data, freq):
    return data.groupby(pd.Grouper(key="czas", freq=freq)).mean()
