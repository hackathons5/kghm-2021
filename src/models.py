""" Models module


"""
import skopt

from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.metrics import precision_score, f1_score, recall_score
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC

from xgboost import XGBClassifier
from lightgbm import LGBMClassifier


from abc import ABC, abstractmethod


class Model(ABC):
    """Abstract model class

    Model class that contains properties

    - model_class: model class from another package (sklearn, xgboost, etc.)
    - name: name of the model

    Model class that contains methods

    - get_search_params: params that will be optimized during the training process
    - get_default_params: params that will be the same during the training process
    - create_model_params: function that combines theese two and creates params that
        will be passed to the init of self.model_class
    """

    @abstractmethod
    def get_search_params(self):
        """Method that returns params that will be optimized during the training process

        Returns:
            list[skopt.space]: list of params to optimize
        """
        pass

    @abstractmethod
    def get_default_params(self):
        """Method that returns params that won't change during the training process

        Returns:
            dict: dict of default params
        """
        pass

    def create_model_params(self, params):
        """Method that combines default and search params into one

        Returns:
            dict: params that can be passed as **kwargs to the init of self.model_class
        """
        return params

    @abstractmethod
    def train(self, X_train, y_train, train_params):
        """Train method of the model

        After it trains the model it saves a trained model as a self.trained_model
            property

        Args:
            X_train (pd.DataFrame): Array with train features
            y_train (pd.DataFrame): Array with train labels
            train_params (dict): params that can be passed
                as **kwargs to the init the model
        """
        pass

    @abstractmethod
    def predict(self, X_val):
        """Predict method of the model

        Args:
            X_val (pd.DataFrame): Array with features to predict
        """
        pass

    def find_best_params(
        self,
        X_train,
        y_train,
        X_val,
        y_val,
        metric="f1",
        n_calls=100,
        n_random_starts=10,
        n_jobs=10,
    ):
        """[summary]

        Args:
            X_train (pd.DataFrame): Array with train features
            y_train (pd.DataFrame): Array with train labels
            X_val (pd.DataFrame): Array with val features
            y_val (pd.DataFrame): Array with val labels
            metric (str, optional): Name of the metric that we will optimize against.
                Possible choices are:

                - accuracy
                - f1
                - recall
                - precision

                Defaults to "f1".
            n_calls (int, optional): The amount of runs done by skopt. Defaults to 100.
            n_random_starts (int, optional): The amount of random starts done by skopt.
                Defaults to 10.
            n_jobs (int, optional): The amount of jobs done by skopt. Defaults to 10.

        Returns:
            [type]: [description]
        """

        standard_params = self.get_default_params()
        search_space = self.get_search_params()

        @skopt.utils.use_named_args(search_space)
        def objective(**params):
            train_params = self.create_model_params(dict(standard_params, **params))
            self.train(X_train, y_train, train_params)
            preds = self.predict(X_val)
            metrics = {
                "accuracy": (preds == y_val).mean(),
                "precision": precision_score(
                    preds,
                    y_val,
                    average="macro",
                    zero_division=1,
                ),
                "recall": recall_score(
                    preds,
                    y_val,
                    average="macro",
                    zero_division=1,
                ),
                "f1": f1_score(
                    preds,
                    y_val,
                    average="macro",
                    zero_division=1,
                ),
            }

            return -metrics[metric]

        search_results = skopt.forest_minimize(
            objective,
            search_space,
            n_calls=2,
            n_random_starts=1,
            n_jobs=1,
        )

        best_params = dict(
            zip([param.name for param in search_space], search_results.x)
        )

        return self.create_model_params(dict(standard_params, **best_params))


class SKLearnModel(Model):
    """Parent class to all the Sklearn models. It specifies the train and predict
    methods
    """

    def train(self, X_train, y_train, params):
        model = self.model_class(**params)
        model.fit(X_train, y_train)
        self.trained_model = model

    def predict(self, X_val):
        return self.trained_model.predict(X_val)


class XGBoost(SKLearnModel):
    """XGBoost model"""

    model_class = XGBClassifier
    name = "xgboost"

    @staticmethod
    def get_search_params():
        return [
            skopt.space.Integer(50, 150, name="n_estimators"),
            skopt.space.Integer(2, 20, name="max_depth"),
            skopt.space.Real(0, 1, name="gamma", prior="uniform"),
            skopt.space.Real(0, 2, name="min_child_weight", prior="uniform"),
            skopt.space.Real(0, 1, name="max_delta_step", prior="uniform"),
            skopt.space.Real(0, 1, name="subsample", prior="uniform"),
        ]

    @staticmethod
    def get_default_params():
        return {
            "objective": "binary:logistic",
            "eval_metric": "auc",
            "use_label_encoder": False,
            "random_state": 42,
            "booster": "gbtree",
        }


class KNN(SKLearnModel):
    """KNN model"""

    model_class = KNeighborsClassifier
    name = "knn"

    def get_search_params(self):
        return [
            skopt.space.Integer(3, 10, name="n_neighbors"),
            skopt.space.Integer(1, 4, name="p"),
            skopt.space.Categorical(["uniform", "distance"], name="weights"),
            skopt.space.Categorical(
                ["auto", "ball_tree", "kd_tree", "brute"], name="algorithm"
            ),
        ]

    def get_default_params(self):
        return dict()


class SVC(SKLearnModel):
    """SVC model"""

    model_class = SVC
    name = "svc"

    def get_search_params(self):
        return [
            skopt.space.Real(0.5, 2, name="C", prior="log-uniform"),
            skopt.space.Categorical(
                ["linear", "poly", "rbf", "sigmoid"], name="kernel"
            ),
            skopt.space.Integer(2, 5, name="degree"),
        ]

    def get_default_params(self):
        return {
            "probability": True,
            "class_weight": "balanced",
        }


class Gauss(SKLearnModel):
    """GaussianProcessClassifier model"""

    model_class = GaussianProcessClassifier
    name = "gauss"

    def get_search_params(self):
        return [
            skopt.space.Integer(0, 3, name="n_restarts_optimizer"),
        ]

    def get_default_params(self):
        return dict()


class MLP(SKLearnModel):
    """MLPClassifier model"""

    model_class = MLPClassifier
    name = "mlp"

    def get_search_params(self):
        return [
            skopt.space.Integer(1, 4, name="hidden_layer_amount"),
            skopt.space.Integer(10, 50, name="1_layer_size"),
            skopt.space.Integer(10, 50, name="2_layer_size"),
            skopt.space.Integer(10, 50, name="3_layer_size"),
            skopt.space.Integer(10, 50, name="4_layer_size"),
            skopt.space.Categorical(
                ["identity", "logistic", "tanh", "relu"], name="activation"
            ),
            skopt.space.Categorical(["sgd", "adam"], name="solver"),
            skopt.space.Real(0.00001, 0.001, name="alpha", prior="log-uniform"),
        ]

    def get_default_params(self):
        return {
            "max_iter": 500,
            "random_state": 42,
        }

    def create_model_params(self, params):
        final_params = {
            key: value for key, value in params.items() if "layer" not in key
        }
        hidden_layer_sizes = tuple(
            [params[f"{i+1}_layer_size"] for i in range(params["hidden_layer_amount"])]
        )

        return dict(final_params, **{"hidden_layer_sizes": hidden_layer_sizes})


class LGBM(SKLearnModel):
    """Light GBM model"""

    model_class = LGBMClassifier
    name = "lgbm"

    @staticmethod
    def get_search_params():
        return [
            skopt.space.Integer(20, 50, name="num_leaves"),
            skopt.space.Integer(0, 5, name="max_depth"),
            skopt.space.Integer(70, 120, name="n_estimators"),
            skopt.space.Integer(10, 30, name="min_child_samples"),
            skopt.space.Real(0.7, 1, name="subsample", prior="uniform"),
            skopt.space.Real(0.7, 1, name="colsample_bytree", prior="uniform"),
            skopt.space.Categorical(["gbdt", "dart", "goss"], name="boosting_type"),
        ]

    @staticmethod
    def get_default_params():
        return {
            "objective": "binary",
            "class_weight": "balanced",
            "random_state": 42,
        }
